package fotokita;

import java.util.UUID;

import javax.xml.bind.annotation.XmlAttribute;


public class Frameholder{

	private String name;
	UUID uuid;
    private double x, y, w, h;
	private String gambar;
	
    public Frameholder() {}  
	public Frameholder(String name, UUID uuid, Double x, Double y, Double w, Double h, String gambar) {  
	    super();  
	    this.name = name;
	    this.uuid = uuid;
	    this.x = x;
	    this.y = y;
	    this.w = w;
	    this.h = h;
	    this.gambar = gambar;
	} 
	@XmlAttribute(name = "name")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@XmlAttribute(name = "uuid")
	public UUID getUUID() {
		return uuid;
	}
	public void setUUID(UUID uuid) {
		this.uuid = uuid;
	}
	
    @XmlAttribute(name= "x")
	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

    @XmlAttribute(name= "y")
	public double getY() {
		return y;
	}

	public void setY(double d) {
		this.y = d;
	}
	
    @XmlAttribute(name= "w")
	public double getW() {
		return w;
	}

	public void setW(double w) {
		this.w = w;
	}

    @XmlAttribute(name= "h")
	public double getH() {
		return h;
	}

	public void setH(double d) {
		this.h = d;
	}

	public void setImage(String base){ 
		this.gambar = base;
	}
	public String getImage(){
    	return gambar;
	}
}