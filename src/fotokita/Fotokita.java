package fotokita;

import java.util.Date;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="fotokita")
public class Fotokita {

	private String judul, versi, auth;
	private int page;
	private Date created, modified;
	private Pages pagess;
	public Fotokita() {}  
	public Fotokita(Date created, Date modified, String versi, String judul, String auth, int page, Pages pagess) {  
	    super();  
	    this.created = created;
	    this.modified = modified;
	    this.versi = versi;
	    this.judul = judul;
	    this.auth = auth;
	    this.page = page;
	    this.pagess = pagess;
	}
//	version attribute
    @XmlAttribute(name="version")
    public String getVersion()
    {
        return versi;
    }
    public void setVersion(String versi) {  
        this.versi = versi;  
    }  
    
//    title
    @XmlElement(name="title")
    public String getTitle()
    {
    	return judul;
    }
    public void setTitle(String judul){
    	this.judul = judul;
    }

//    author
    @XmlElement(name="author")
    public String getAuthor(){
    	return auth;
    }
    public void setAuthor(String auth){
    	this.auth = auth;
    }
    
//    canvas
    @XmlElement(name="pageamount")
    public int getPageAmount(){
    	return page;
    }
    public void setPageAmount(int page_amount){
    	this.page = page_amount;
    }


//	Date created
	@XmlElement(name="createdat")
	public void setDateCreated(Date created){
		this.created = created;
	}
	public Date getDateCreated(){
		return created;
	}
    
//    lastmodified
	@XmlElement(name="lastmodified")
	public void setLastModified(Date last_modified){
		this.modified = last_modified;
	}
	public Date getLastModified(){
		return modified;
	}
    
//	Pages
    @XmlElement(name="pages")
	public Pages getPages(){
		return pagess;
	}
	public void setPages(Pages pagess){
		this.pagess = pagess;
	}
}