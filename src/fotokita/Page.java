package fotokita;
import java.util.List;
import java.util.UUID;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

public class Page {
	private Integer page;
    private String layout;
    private List<Paragraph> text;
    private List<Photoholder> photoholder;
    private List<Frameholder> frameholder;
	private UUID uuid;
	public Page() {}  
	public Page(List<Photoholder> photoholder,List<Frameholder> frameholder, String layout, List<Paragraph> text, UUID uuid, Integer page) {  
	    super();  
	    this.photoholder = photoholder;  
	    this.frameholder = frameholder;  
	    this.layout = layout;
	    this.text = text;
	    this.uuid = uuid;
	    this.page = page;
	} 
	@XmlAttribute(name= "no")
	public Integer getPage() {
		return page;
	}

	public void setPage(Integer page) {
		this.page = page;
	}

	@XmlElement(name="photoholder")
    protected List<Photoholder> getPhotoHolder() {
        return photoholder;
    }
 
    public void setPhotoHolder(List<Photoholder> photo) {
        this.photoholder = photo;
    }

	@XmlElement(name="frameholder")
    protected List<Frameholder> getFrameHolder() {
        return frameholder;
    }
 
    public void setFrameHolder(List<Frameholder> frame) {
        this.frameholder = frame;
    }
  
	@XmlElement(name="text")
	protected List<Paragraph> getText() {
			return text;
	}

	public void setText(List<Paragraph> text) {
		this.text = text;
	}

    @XmlElement(name="layout")
    public String getLayout(){
    	return layout;
    }
    
    public void setLayout(String layout){
    	this.layout = layout;
    }
    
	@XmlAttribute(name = "uuid")
	public UUID getUUID() {
		return uuid;
	}
	public void setUUID(UUID uuid) {
		this.uuid = uuid;
	}

}