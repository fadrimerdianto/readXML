package fotokita;
import java.util.List;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

public class Pages {
	private String pageSize;
    private List<Page> halaman;
	public Pages() {}  
	public Pages(List<Page> halaman, String pageSize) {  
	    super();  
	    this.halaman = halaman;  
	    this.pageSize = pageSize; 
	} 
	@XmlElement(name="page")
    protected List<Page> getPages() {
        return halaman;
    }
 
    public void setPages(List<Page> pages) {
        this.halaman = pages;
    }
    @XmlAttribute(name= "size")
	public String getSize() {
		return pageSize;
	}

	public void setSize(String pageSize) {
		this.pageSize = pageSize;
	}

}