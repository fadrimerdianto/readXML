package fotokita;


import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlValue;

public class Paragraph {
	private String text, font, color, style;
	private Integer size;
	private double x, y;
	
	public Paragraph() {}  
	public Paragraph(String text, String font, String color, Integer size, double x, double y, String style) {  
	    super();  
	    this.text = text;
	    this.font = font;
	    this.color = color;
	    this.size = size;
	    this.x = x;
	    this.y = y;
	    this.style = style;
	} 
	@XmlValue()
	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

//	Font
	
	@XmlAttribute(name = "font")
	public String getFont() {
		return font;
	}

	public void setFont(String font) {
		this.font = font;
	}
	
//	Font Color
	
	@XmlAttribute(name="color")
	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}
	
//	Size
	
	@XmlAttribute(name="size")
	public Integer getSize() {
		return size;
	}

	public void setSize(Integer size) {
		this.size = size;
	}
	
//	x
	@XmlAttribute(name="x")
	public double getX(){
		return x;
	}
	
	public void setX(double x){
		this.x = x;
	}
	
//	y
	@XmlAttribute(name="y")
	public double getY(){
		return y;
	}
	
	public void setY(double y){
		this.y = y;
	}	
	
//	style
	@XmlAttribute(name="style")
	public String getStyle(){
		return style;
	}
	
	public void setStyle(String style){
		this.style = style;
	}
}