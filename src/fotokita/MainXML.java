package fotokita;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.xml.XMLConstants;
import javax.xml.bind.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.apache.commons.codec.binary.Base64;
import org.eclipse.persistence.jaxb.MarshallerProperties;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class MainXML {
	static Fotokita xml = new Fotokita();
	public static void main(String[] args) throws Exception {
		final Display display = new Display();
		// Allocation and initialization
		final Shell shell = new Shell(display, SWT.SHELL_TRIM | SWT.CENTER);
		// create layout
		GridLayout layout = new GridLayout(2, false);
		shell.setLayout(layout);
		shell.setText("Fotokita");

		// Button Create
		Button create_xml = new Button(shell, SWT.PUSH);
		create_xml.setText("Create XML");
		create_xml.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				try {
					marshal(display);
				} catch (JAXBException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				// TODO Auto-generated method stub
			}

		});

		// Button Load
		Button load_xml = new Button(shell, SWT.PUSH);
		load_xml.setText("Load XML");
		load_xml.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				try {
					unmarshal(display);
				} catch (JAXBException | ParserConfigurationException | SAXException | IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				// TODO Auto-generated method stub
			}

		});

		// Button XPATH
		Button load_xpath = new Button(shell, SWT.PUSH);
		load_xpath.setText("Load XPATH");
		load_xpath.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				try {
					unmarshalling(display);
				} catch (JAXBException | ParserConfigurationException | SAXException | IOException
						| XPathExpressionException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				// TODO Auto-generated method stub
			}

		});

		// Button XPATH
		Button btn_cek = new Button(shell, SWT.PUSH);
		btn_cek.setText("cek skema");
		btn_cek.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				try {
					cekSchema();
				} catch (JAXBException | SAXException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				// TODO Auto-generated method stub
			}

		});
		// Button Update
		Button update_xml = new Button(shell, SWT.PUSH);
		update_xml.setText("Update XML");
		update_xml.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				try {
					update(display);
				} catch (ParserConfigurationException | SAXException | IOException | JAXBException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				// TODO Auto-generated method stub
			}

		});

		shell.pack();
		shell.open();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		display.dispose();
	}

	public static void cekSchema() throws SAXException, JAXBException{
		SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
		Schema schema = schemaFactory.newSchema(new File("/Project/COBATA/fotokitaxsd.xsd"));
//        Schema schema = schemaFactory.newSchema(new File("/Project/COBATA/customer.xsd")); 
        
		JAXBContext xpath_jc = JAXBContext.newInstance(Fotokita.class);
//        JAXBContext xpath_jc = JAXBContext.newInstance(Customer.class);
        
        Unmarshaller unmarshaller = xpath_jc.createUnmarshaller();
		unmarshaller.setSchema(schema);
		unmarshaller.setEventHandler(new MyValidationEventHandler());
		try {
			Fotokita fotokita_xml_schema= (Fotokita) unmarshaller.unmarshal(new File("/Project/COBATA/fotokita100.fbk"));
		} catch (JAXBException e1) {
			// TODO Auto-generated catch block
//			e1.printStackTrace();
			System.out.println("BEDA");
		}
//        Customer customer = (Customer) unmarshaller.unmarshal(new File("/Project/COBATA/cobainput.xml"));
	}
	
	public static void unmarshalling(final Display display)
			throws SAXException, IOException, XPathExpressionException, ParserConfigurationException, JAXBException {
		DocumentBuilderFactory dbf1 = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder1 = dbf1.newDocumentBuilder();
        Document fotokita_xml = docBuilder1.parse("/Project/COBATA/fotokita100.fbk");

		SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
		cekSchema();
		XPath xPath = XPathFactory.newInstance().newXPath();
		String xpath = "/fotokita";
		fotokita_xml.getDocumentElement().normalize();
		System.out.println("Root element\t: " + fotokita_xml.getDocumentElement().getNodeName());

		// print author
		String author_xpath = xpath + "/author";
		Node node_author = (Node) xPath.compile(author_xpath).evaluate(fotokita_xml, XPathConstants.NODE);
		System.out.println(node_author.getNodeName() + "\t\t: " + node_author.getFirstChild().getNodeValue());

		// print versi
		String versi_xpath = xpath + "/@version";
		Node node_versi = (Node) xPath.compile(versi_xpath).evaluate(fotokita_xml, XPathConstants.NODE);
		System.out.println(node_versi.getNodeName() + "\t\t: " + node_versi.getFirstChild().getNodeValue());

		// print title
		String title_xpath = xpath + "/title";
		Node node_title = (Node) xPath.compile(title_xpath).evaluate(fotokita_xml, XPathConstants.NODE);
		System.out.println(node_title.getNodeName() + "\t\t: " + node_title.getFirstChild().getNodeValue());
		
		// print title
		String pageamount_xpath = xpath + "/pageamount";
		Node node_pageamount = (Node) xPath.compile(pageamount_xpath).evaluate(fotokita_xml, XPathConstants.NODE);
		System.out.println(node_pageamount.getNodeName() + "\t\t: " + node_pageamount.getFirstChild().getNodeValue());

		// print created
		String createdat_xpath = xpath + "/createdat";
		Node node_created = (Node) xPath.compile(createdat_xpath).evaluate(fotokita_xml, XPathConstants.NODE);
		System.out.println(node_created.getNodeName() + "\t: " + node_created.getFirstChild().getNodeValue());

		// print title
		String lastmodified_xpath = xpath + "/lastmodified";
		Node node_lastmodified = (Node) xPath.compile(lastmodified_xpath).evaluate(fotokita_xml, XPathConstants.NODE);
		System.out.println(node_lastmodified.getNodeName() + "\t: " + node_lastmodified.getFirstChild().getNodeValue());

		String pages_xpath = xpath + "/pages";

		// print pagesize
		String pagesize_xpath = pages_xpath + "/@size";
		Node node_pagesize = (Node) xPath.compile(pagesize_xpath).evaluate(fotokita_xml, XPathConstants.NODE);
		System.out.println(node_pagesize.getNodeName() + "\t\t: " + node_pagesize.getFirstChild().getNodeValue());
		String page_xpath = pages_xpath + "/page";
		// String pagenumber_xpath = page_xpath + "/@no";
		// Node node_page_number = (Node)
		// xPath.compile(pagenumber_xpath).evaluate(fotokita_xml,
		// XPathConstants.NODE);
		NodeList nodeList_page = (NodeList) xPath.compile(page_xpath).evaluate(fotokita_xml, XPathConstants.NODESET);
		System.out.println("Page amount\t: " + nodeList_page.getLength());
		for (int i = 0; i < nodeList_page.getLength(); i++) {
			Node node_page = nodeList_page.item(i);
			System.out.println("\nCurrent Element : " + node_page.getNodeName());
			if (node_page.getNodeType() == Node.ELEMENT_NODE) {
				Element page_element = (Element) node_page;
				System.out.println("Page Number\t: " + ((Element) page_element).getAttribute("no"));
				System.out.println("Page UUID\t: " + ((Element) page_element).getAttribute("uuid"));
				// ((Element)
				// page_element).getElementsByTagName("layout").item(0).setTextContent("Ini
				// Layout");
				String layout_xpath = page_xpath + "[@no='" + i + "']/layout";
				Node node_layout = (Node) xPath.compile(layout_xpath).evaluate(fotokita_xml, XPathConstants.NODE);
				System.out.println(node_layout.getNodeName() + "\t\t: " + node_layout.getFirstChild().getNodeValue());
				// System.out.println("Layout\t\t: " + ((Element)
				// page_element).getElementsByTagName("layout").item(0).getTextContent());
				String frameholder_xpath = page_xpath + "[@no='" + i + "']/frameholder";
				// String frameholder_xpath = page_xpath +
				// "[@uuid='0a34078f-f763-4da1-99c0-8faba5578f2b']/frameholder";
				String photoholder_xpath = page_xpath + "[@no='" + i + "']/photoholder";
				String text_xpath = page_xpath + "[@no='" + i + "']/text";
				NodeList nodeList_frameholder = (NodeList) xPath.compile(frameholder_xpath).evaluate(fotokita_xml,
						XPathConstants.NODESET);
				for (int j = 0; j < nodeList_frameholder.getLength(); j++) {
					Node node_frameholder = nodeList_frameholder.item(j);
					if (node_frameholder.getNodeType() == Node.ELEMENT_NODE) {
						Element frameholder_element = (Element) node_frameholder;
						// String frameholder_attribute = frameholder_xpath +
						// "[@uuid'"+ String.valueOf(((Element)
						// frameholder_element).getAttribute("uuid"))+"']";
						// String frameholder_attribute = frameholder_xpath +
						// "[@uuid='824c51ef-1173-4fbf-8d2d-e3687009e7eb']";
						System.out.println(
								("\tFrameholder UUID\t: " + ((Element) frameholder_element).getAttribute("uuid")));
						if (((Element) frameholder_element).getAttribute("uuid")
								.equals("824c51ef-1173-4fbf-8d2d-e3687009e7eb")) {
							((Element) frameholder_element).setAttribute("name", "Bude");
						}
						System.out.println(
								("\tFrameholder Name\t: " + ((Element) frameholder_element).getAttribute("name")));
						System.out
								.println(("\tFrameholder w\t\t: " + ((Element) frameholder_element).getAttribute("w")));
						System.out
								.println(("\tFrameholder x\t\t: " + ((Element) frameholder_element).getAttribute("x")));
						System.out
								.println(("\tFrameholder y\t\t: " + ((Element) frameholder_element).getAttribute("y")));
						System.out
								.println(("\tFrameholder h\t\t: " + ((Element) frameholder_element).getAttribute("h")));
						String frameholder_image = frameholder_xpath + "/image";
						Node node_frameholder_image = (Node) xPath.compile(frameholder_image).evaluate(fotokita_xml,
								XPathConstants.NODE);
						System.out.println("\tImage h\t\t\t: " + node_frameholder_image.getFirstChild().getNodeValue());
					}
				}
				NodeList nodeList_photoholder = (NodeList) xPath.compile(photoholder_xpath).evaluate(fotokita_xml,
						XPathConstants.NODESET);
				for (int j = 0; j < nodeList_photoholder.getLength(); j++) {
					Node node_photoholder = nodeList_photoholder.item(j);
					if (node_photoholder.getNodeType() == Node.ELEMENT_NODE) {
						Element photoholder_element = (Element) node_photoholder;
						System.out.println(
								("\tPhotoholder UUID\t: " + ((Element) photoholder_element).getAttribute("uuid")));
						System.out.println(
								("\tPhotoholder Name\t: " + ((Element) photoholder_element).getAttribute("name")));
						System.out
								.println(("\tPhotoholder w\t\t: " + ((Element) photoholder_element).getAttribute("w")));
						System.out
								.println(("\tPhotoholder x\t\t: " + ((Element) photoholder_element).getAttribute("x")));
						System.out
								.println(("\tPhotoholder y\t\t: " + ((Element) photoholder_element).getAttribute("y")));
						System.out
								.println(("\tPhotoholder h\t\t: " + ((Element) photoholder_element).getAttribute("h")));
						String photoholder_image = photoholder_xpath + "/image";
						Node node_photoholder_image = (Node) xPath.compile(photoholder_image).evaluate(fotokita_xml,
								XPathConstants.NODE);
						System.out.println("\tImage h\t\t\t: " + node_photoholder_image.getFirstChild().getNodeValue());
					}
				}
				NodeList nodeList_text = (NodeList) xPath.compile(text_xpath).evaluate(fotokita_xml,
						XPathConstants.NODESET);
				for (int j = 0; j < nodeList_text.getLength(); j++) {
					Node node_text = nodeList_text.item(j);
					if (node_text.getNodeType() == Node.ELEMENT_NODE) {
						Element text_element = (Element) node_text;
						System.out.println(("\tText Color\t\t: " + ((Element) text_element).getAttribute("color")));
						System.out.println(("\tText Font\t\t: " + ((Element) text_element).getAttribute("font")));
						System.out.println(("\tText Size\t\t: " + ((Element) text_element).getAttribute("size")));
						System.out.println(("\tText x\t\t\t: " + ((Element) text_element).getAttribute("x")));
						System.out.println(("\tText y\t\t\t: " + ((Element) text_element).getAttribute("y")));
						;
						System.out.println(("\tText Content\t\t: " + ((Element) text_element).getTextContent()));
					}
				}
			}
			// System.out.println("\nCurrent Element :" +
			// node_page.getNodeName());
			// System.out.println(node_page.getNodeName() + ":\t" +
			// node_page_number.getFirstChild().getNodeValue());
		}
		// String layout_xpath = page_xpath + "/layout";
		// NodeList nodeLayout_xpath = (NodeList)
		// xPath.compile(layout_xpath).evaluate(fotokita_xml,
		// XPathConstants.NODESET);
		// for (int i = 0; i < nodeLayout_xpath.getLength(); i++) {
		//
		// Node nNode = nodeList.item(i);
		// System.out.println("\nCurrent Element :"
		// + nNode.getNodeName());
		// System.out.println("Layout:\t" +
		// nodeLayout_xpath.item(i).getFirstChild().getNodeValue());
		// }
	}

	public static void marshal(final Display display) throws JAXBException {
		// Allocation and initialization
		final Shell dialog_create = new Shell(display, SWT.SHELL_TRIM | SWT.CENTER);

		// create layout
		GridLayout layout = new GridLayout(2, false);
		dialog_create.setLayout(layout);
		dialog_create.setText("Fotokita");

		// version
		Label l_version = new Label(dialog_create, SWT.NONE);
		l_version.setText("version");
		final Text tb_version = new Text(dialog_create, SWT.NONE);

		// title
		Label l_title = new Label(dialog_create, SWT.NONE);
		l_title.setText("title");
		final Text tb_title = new Text(dialog_create, SWT.NONE);

		// author
		Label l_author = new Label(dialog_create, SWT.NONE);
		l_author.setText("author");
		final Text tb_author = new Text(dialog_create, SWT.NONE);

		// page size
		Label l_pagesize = new Label(dialog_create, SWT.NONE);
		l_pagesize.setText("page size");
		final Text tb_pagesize = new Text(dialog_create, SWT.NONE);

		// page
		Label l_page = new Label(dialog_create, SWT.NONE);
		l_page.setText("page");
		final Text tb_page = new Text(dialog_create, SWT.NONE);

		// Button tambah page
		Button btn_addpage = new Button(dialog_create, SWT.PUSH);
		btn_addpage.setText("Tambah page");
		btn_addpage.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				Pages pages = new Pages();
				String pageSize = tb_pagesize.getText();
				pages.setPages(new ArrayList<Page>());
				pages.setSize(pageSize);
				if (tb_page.getText() != null && !tb_page.getText().equals("")) {
					pagenumber(display, tb_page.getText(), pages);
				} else {
					System.out.println("Masukkan jumlah halaman");
				}
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				// TODO Auto-generated method stub
			}

		});

		// Button OK
		Button btn_ok = new Button(dialog_create, SWT.PUSH);
		btn_ok.setText("OK");
		btn_ok.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				String titleName = tb_title.getText();
				int pageAmount = Integer.valueOf(tb_page.getText());
				String authorName = tb_author.getText();
				String versionName = tb_version.getText();
				Date dateCreated = new Date();
				xml.setDateCreated(dateCreated);
				Date dateModified = new Date();
				xml.setLastModified(dateModified);
				xml.setPageAmount(pageAmount);
				xml.setAuthor(authorName);
				xml.setTitle(titleName);
				xml.setVersion(versionName);
				xml.getDateCreated();
				xml.getLastModified();
				try {
					marshalling();
				} catch (JAXBException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (SAXException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				// TODO Auto-generated method stub

			}
		});
		dialog_create.pack();
		dialog_create.open();
		while (!dialog_create.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
	}

	public static void pagenumber(Display display, String number, final Pages pages) {
		int i;
		for (i = 0; i < Integer.parseInt(number); i++) {
			// Allocation and initialization
			final Shell dialog_page = new Shell(display, SWT.SHELL_TRIM | SWT.CENTER);

			System.out.println(i);
			GridLayout layout = new GridLayout(2, false);
			dialog_page.setLayout(layout);
			dialog_page.setText("Page " + i);
			// page number
			Label l_text = new Label(dialog_page, SWT.NONE);
			l_text.setText("text");
			final Text tb_text = new Text(dialog_page, SWT.NONE);

			Label l_textcolor = new Label(dialog_page, SWT.NONE);
			l_textcolor.setText("text color");
			final Text tb_text_color = new Text(dialog_page, SWT.NONE);

			Label l_textfont = new Label(dialog_page, SWT.NONE);
			l_textfont.setText("text font");
			final Text tb_text_font = new Text(dialog_page, SWT.NONE);

			Label l_textsize = new Label(dialog_page, SWT.NONE);
			l_textsize.setText("text size");
			final Text tb_text_size = new Text(dialog_page, SWT.NONE);

			Label l_textstyle = new Label(dialog_page, SWT.NONE);
			l_textstyle.setText("text style");
			final Text tb_text_style = new Text(dialog_page, SWT.NONE);

			Label l_layout = new Label(dialog_page, SWT.NONE);
			l_layout.setText("Layout");
			final Text tb_layout = new Text(dialog_page, SWT.NONE);

			final int page_n = i;

			Button btn_tambah = new Button(dialog_page, SWT.PUSH);
			btn_tambah.setText("Tambahkan");
			btn_tambah.addSelectionListener(new SelectionListener() {

				@Override
				public void widgetSelected(SelectionEvent e) {
					// TODO Auto-generated method stub

					Page page = new Page();
					page.setLayout(tb_layout.getText());
					page.setPage(page_n);
					page.setUUID(UUID.randomUUID());
					page.setPhotoHolder(new ArrayList<Photoholder>());
					Photoholder photoholder = new Photoholder();
					photoholder.setName("Photo Holder " + page_n);
					photoholder.setUUID(UUID.randomUUID());
					photoholder.setH(7);
					photoholder.setW(4);
					photoholder.setX(7);
					photoholder.setY(8);
					photoholder.setImage(toBase64("E:/Project/COBATA/isyana.jpg"));
					page.getPhotoHolder().add(photoholder);
					page.setFrameHolder(new ArrayList<Frameholder>());
					Frameholder frameholder = new Frameholder();
					frameholder.setName("Frame Holder " + page_n);
					frameholder.setUUID(UUID.randomUUID());
					frameholder.setH(12);
					frameholder.setW(12);
					frameholder.setX(10);
					frameholder.setY(10);
					frameholder.setImage(toBase64("E:/Project/COBATA/zigzag.png"));
					page.getFrameHolder().add(frameholder);
					page.setText(new ArrayList<Paragraph>());
					Paragraph text1 = new Paragraph();
					text1.setText(tb_text.getText());
					text1.setColor(tb_text_color.getText());
					text1.setFont(tb_text_font.getText());
					text1.setStyle(tb_text_style.getText());
					text1.setSize(Integer.valueOf(tb_text_size.getText()));
					text1.setX(0);
					text1.setY(0);
					page.getText().add(text1);
					pages.getPages().add(page);
					dialog_page.close();
				}

				@Override
				public void widgetDefaultSelected(SelectionEvent e) {
					// TODO Auto-generated method stub

				}

			});
			xml.setPages(pages);

			dialog_page.pack();
			dialog_page.open();
			while (!dialog_page.isDisposed()) {
				if (!display.readAndDispatch()) {
					display.sleep();
				}
			}
		}

	}

	public static void marshalling() throws JAXBException, SAXException {

		File file = new File("/Project/COBATA/fotokita3.fbk");
		JAXBContext jaxbContext = JAXBContext.newInstance(Fotokita.class);
		Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

		SchemaFactory sf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI); 
        Schema schema = sf.newSchema(new File("/Project/COBATA/fotokitaxsd.xsd"));
		jaxbMarshaller.setSchema(schema);
		jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		jaxbMarshaller.marshal(xml, file);
		jaxbMarshaller.marshal(xml, System.out);
	}

	public static void unmarshal(Display display)
			throws JAXBException, ParserConfigurationException, SAXException, IOException {
		JAXBContext jc1 = JAXBContext.newInstance(Fotokita.class);
		Unmarshaller unmarshaller = jc1.createUnmarshaller();
		DocumentBuilderFactory dbf1 = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder1 = dbf1.newDocumentBuilder();
		Document fotokita_xml = docBuilder1.parse("/Project/COBATA/fotokita3.fbk");
		// File fotokita_xml = new File("/Project/COBATA/fotokita3.fbk");
		final Fotokita fotokita = (Fotokita) unmarshaller.unmarshal(fotokita_xml);
		Binder<Node> binder = jc1.createBinder();
		// get xml node from the document
		final Node xmlNode = fotokita_xml.getDocumentElement();

		// Returns the updated JAXB object
		final Fotokita fk = (Fotokita) binder.updateJAXB(xmlNode);

		fk.setLastModified(new Date());

		System.out.println("versi: " + fotokita.getVersion());
		System.out.println("judul: " + fotokita.getTitle());
		System.out.println("author: " + fotokita.getAuthor());
		System.out.println("canvas: " + fotokita.getPageAmount());
		System.out.println("createdat: " + fotokita.getDateCreated());
		System.out.println("lastmodified: " + fotokita.getLastModified());
		Pages pagess = fotokita.getPages();
		System.out.println("pagesize: " + pagess.getSize());
		List<Page> list = pagess.getPages();
		System.out.println("Pages:");
		for (Page pglist : list) {
			System.out.println("--------------------");
			System.out.println("page number: " + pglist.getPage());
			System.out.println("page uuid: " + pglist.getUUID());
			System.out.println("page layout: " + pglist.getLayout());
			List<Photoholder> photoholder_list = pglist.getPhotoHolder();
			List<Frameholder> frameholder_list = pglist.getFrameHolder();
			List<Paragraph> txtlist = pglist.getText();
			System.out.println("++++++++++++++++++++");
			if (photoholder_list != null) {
				System.out.println("Object:");
				for (Photoholder obj : photoholder_list) {
					System.out.println("********************");
					System.out.println("uuid: " + obj.getUUID());
					System.out.println("name: " + obj.getName());
					System.out.println("x: " + obj.getX());
					System.out.println("y: " + obj.getY());
					System.out.println("h: " + obj.getH());
					System.out.println("w: " + obj.getW());
					System.out.println("Image: " + obj.getImage());
					if (obj.getImage() != null) {
						toImage(obj.getImage());
					}
					// System.out.println("gambar: " +
					// obj.getImage().getImages());
				}
			} else {
				System.out.println("Object empty");
			}
			if (frameholder_list != null) {
				System.out.println("Frame:");
				for (Frameholder frame : frameholder_list) {
					System.out.println("********************");
					System.out.println("uuid: " + frame.getUUID());
					System.out.println("name: " + frame.getName());
					System.out.println("x: " + frame.getX());
					System.out.println("y: " + frame.getY());
					System.out.println("h: " + frame.getH());
					System.out.println("w: " + frame.getW());
					System.out.println("Image: " + frame.getImage());
					if (frame.getImage() != null) {
						toImage(frame.getImage());
					}
					// System.out.println("gambar: " +
					// obj.getImage().getImages());
				}
			} else {
				System.out.println("Object empty");
			}
			System.out.println("++++++++++++++++++++");
			if (txtlist != null) {
				System.out.println("Text:");
				for (Paragraph txt : txtlist) {
					System.out.println("********************");
					System.out.println("text: " + txt.getText());
					System.out.println("color: " + txt.getColor());
					System.out.println("size: " + txt.getSize());
					System.out.println("font: " + txt.getFont());
				}
			} else {
				System.out.println("Text empty");
			}
		}

		final Shell dialog_read = new Shell(display,
				SWT.SHELL_TRIM | SWT.CENTER | SWT.V_SCROLL | SWT.H_SCROLL | SWT.MULTI);

		GridLayout layout = new GridLayout(2, false);

		dialog_read.setLayout(layout);
		dialog_read.setText("Fotokita");
		// Button OK
		Button btn_update = new Button(dialog_read, SWT.NONE);
		btn_update.setText("UPDATE");

		// version
		Label l_version = new Label(dialog_read, SWT.NONE);
		l_version.setText("version");
		final Text tb_version = new Text(dialog_read, SWT.NONE);
		tb_version.setText(fotokita.getVersion());

		// title
		Label l_title = new Label(dialog_read, SWT.NONE);
		l_title.setText("title");
		final Text tb_title = new Text(dialog_read, SWT.NONE);
		tb_title.setText(fotokita.getTitle());

		// author
		Label l_author = new Label(dialog_read, SWT.NONE);
		l_author.setText("author");
		final Text tb_author = new Text(dialog_read, SWT.NONE);
		tb_author.setText(fotokita.getAuthor());

		// page size
		Label l_pagesize = new Label(dialog_read, SWT.NONE);
		l_pagesize.setText("page size");
		final Text tb_pagesize = new Text(dialog_read, SWT.NONE);
		tb_pagesize.setText(fotokita.getPages().getSize());

		// canvas
		Label l_canvas = new Label(dialog_read, SWT.NONE);
		l_canvas.setText("canvas");
		final Text tb_canvas = new Text(dialog_read, SWT.NONE);
		tb_canvas.setText(String.valueOf(fotokita.getPageAmount()));

		// set versi
		if (!fotokita.getVersion().equals(tb_version.getText())) {
			fk.setVersion(tb_version.getText());
			System.out.println("Version Changed");
		}
		// set author
		if (!fotokita.getAuthor().equals(tb_author.getText())) {
			fk.setAuthor(tb_author.getText());
			System.out.println("Author Changed");
		}
		Pages page_load = fotokita.getPages();
		List<Page> page_load_list = page_load.getPages();
		for (Page page_list : page_load_list) {
			System.out.println("--------------------");
			Label l_page_number = new Label(dialog_read, SWT.NONE);
			l_page_number.setText("page number:");
			final Text tb_load_page_number = new Text(dialog_read, SWT.NONE);
			tb_load_page_number.setText(String.valueOf(page_list.getPage()));
			// System.out.println("page number: " + page_list.getPage());
			Label l_page_uuid = new Label(dialog_read, SWT.NONE);
			l_page_uuid.setText("page " + String.valueOf(page_list.getPage()) + " uuid: ");
			Label val_page_uuid = new Label(dialog_read, SWT.NONE);
			val_page_uuid.setText(String.valueOf(page_list.getUUID()));
			// System.out.println("page uuid: " + page_list.getUUID());
			Label l_page_layout = new Label(dialog_read, SWT.NONE);
			l_page_layout.setText("page layout:");
			final Text tb_load_page_layout = new Text(dialog_read, SWT.NONE);
			tb_load_page_layout.setText(page_list.getLayout());
			// System.out.println("page layout: " + page_list.getLayout());
			List<Photoholder> photoholder_page_list = page_list.getPhotoHolder();
			List<Frameholder> frameholder_page_list = page_list.getFrameHolder();
			List<Paragraph> txtpage_list = page_list.getText();
			// System.out.println("++++++++++++++++++++");
			if (photoholder_page_list != null) {
				// System.out.println("Object:");
				for (Photoholder obj : photoholder_page_list) {
					// System.out.println("********************");
					// System.out.println("uuid: " + obj.getUUID());
					Label l_photoholder_uuid = new Label(dialog_read, SWT.NONE);
					l_photoholder_uuid.setText("Photoholder UUID: ");
					Label val_photoholder_uuid = new Label(dialog_read, SWT.NONE);
					val_photoholder_uuid.setText(String.valueOf(obj.getUUID()));
					// System.out.println("name: " + obj.getName());
					Label l_photoholder_name = new Label(dialog_read, SWT.NONE);
					l_photoholder_name.setText("Photoholder name: ");
					final Text tb_photoholder_name = new Text(dialog_read, SWT.NONE);
					tb_photoholder_name.setText(obj.getName());
					// System.out.println("x: " + obj.getX());
					Label l_photoholder_x = new Label(dialog_read, SWT.NONE);
					l_photoholder_x.setText("Photoholder X:");
					final Text tb_photoholder_x = new Text(dialog_read, SWT.NONE);
					tb_photoholder_x.setText(String.valueOf(obj.getX()));
					// System.out.println("y: " + obj.getY());
					Label l_photoholder_y = new Label(dialog_read, SWT.NONE);
					l_photoholder_y.setText("Photoholder Y:");
					final Text tb_photoholder_y = new Text(dialog_read, SWT.NONE);
					tb_photoholder_y.setText(String.valueOf(obj.getY()));
					// System.out.println("h: " + obj.getH());
					Label l_photoholder_h = new Label(dialog_read, SWT.NONE);
					l_photoholder_h.setText("Photoholder H:");
					final Text tb_photoholder_h = new Text(dialog_read, SWT.NONE);
					tb_photoholder_h.setText(String.valueOf(obj.getH()));
					// System.out.println("w: " + obj.getW());
					Label l_photoholder_w = new Label(dialog_read, SWT.NONE);
					l_photoholder_w.setText("Photoholder W:");
					final Text tb_photoholder_w = new Text(dialog_read, SWT.NONE);
					tb_photoholder_w.setText(String.valueOf(obj.getW()));
					// System.out.println("Image: " + obj.getImage());
					Label l_photoholder_image = new Label(dialog_read, SWT.NONE);
					l_photoholder_image.setText("Photoholder image:");
					final Text tb_photoholder_image = new Text(dialog_read, SWT.NONE);
					tb_photoholder_image.setText(obj.getImage());
					// if(obj.getImage()!= null){
					// toImage(obj.getImage());
					// }
					//// System.out.println("gambar: " +
					// obj.getImage().getImages());
				}
			} else {
				System.out.println("Object empty");
			}
			if (frameholder_page_list != null) {
				// System.out.println("Frame:");
				for (Frameholder frame : frameholder_page_list) {
					// System.out.println("********************");
					// System.out.println("uuid: " + frame.getUUID());
					Label l_frameholder_uuid = new Label(dialog_read, SWT.NONE);
					l_frameholder_uuid.setText("Frameholder UUID: ");
					Label val_frameholder_uuid = new Label(dialog_read, SWT.NONE);
					val_frameholder_uuid.setText(String.valueOf(frame.getUUID()));
					// System.out.println("name: " + frame.getName());
					Label l_frameholder_name = new Label(dialog_read, SWT.NONE);
					l_frameholder_name.setText("Frameholder name: ");
					final Text tb_frameholder_name = new Text(dialog_read, SWT.NONE);
					tb_frameholder_name.setText(frame.getName());
					// System.out.println("x: " + frame.getX());
					Label l_frameholder_x = new Label(dialog_read, SWT.NONE);
					l_frameholder_x.setText("Frameholder X:");
					final Text tb_frameholder_x = new Text(dialog_read, SWT.NONE);
					tb_frameholder_x.setText(String.valueOf(frame.getX()));
					// System.out.println("y: " + frame.getY());
					Label l_frameholder_y = new Label(dialog_read, SWT.NONE);
					l_frameholder_y.setText("Frameholder Y:");
					final Text tb_frameholder_y = new Text(dialog_read, SWT.NONE);
					tb_frameholder_y.setText(String.valueOf(frame.getY()));
					// System.out.println("h: " + frame.getH());
					Label l_frameholder_h = new Label(dialog_read, SWT.NONE);
					l_frameholder_h.setText("Frameholder H:");
					final Text tb_frameholder_h = new Text(dialog_read, SWT.NONE);
					tb_frameholder_h.setText(String.valueOf(frame.getH()));
					// System.out.println("w: " + frame.getW());
					Label l_frameholder_w = new Label(dialog_read, SWT.NONE);
					l_frameholder_w.setText("Frameholder W:");
					final Text tb_frameholder_w = new Text(dialog_read, SWT.NONE);
					tb_frameholder_w.setText(String.valueOf(frame.getW()));
					// System.out.println("Image: " + frame.getImage());
					Label l_frameholder_image = new Label(dialog_read, SWT.NONE);
					l_frameholder_image.setText("Frameholder image:");
					final Text tb_frameholder_image = new Text(dialog_read, SWT.NONE);
					tb_frameholder_image.setText(frame.getImage());
					// if(frame.getImage()!= null){
					// toImage(frame.getImage());
					// }
					//// System.out.println("gambar: " +
					// obj.getImage().getImages());
				}
			}
			// else {
			// System.out.println("Object empty");
			// }
			// System.out.println("++++++++++++++++++++");
			// if(txtlist != null){
			// System.out.println("Text:");
			// for(Paragraph txt:txtlist){
			// System.out.println("********************");
			// System.out.println("text: " + txt.getText());
			// System.out.println("color: " + txt.getColor());
			// System.out.println("size: " + txt.getSize());
			// System.out.println("font: " + txt.getFont());
			// }
			// }
			// else{
			// System.out.println("Text empty");
			// }
		}

		btn_update.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {

				// if(!fotokita.getTitle().equals(tb_title.getText())){
				//// String titleName = tb_title.getText();
				// System.out.println("Title berubah. Awal: " +
				// fotokita.getTitle() + ", akhir: " +tb_title.getText());
				//// System.out.println(tb_title.getText());
				// xml.setTitle(tb_title.getText());
				// xml.setLastModified(new Date());
				// }
				// if(!fotokita.getCanvas().equals(tb_canvas.getText())){
				// System.out.println("Canvas berubah. Awal: " +
				// fotokita.getCanvas() + ", akhir: " +tb_canvas.getText());
				// xml.setCanvas(tb_canvas.getText());
				// xml.setLastModified(new Date());
				// }
				// if(!fotokita.getAuthor().equals(tb_author.getText())){
				// System.out.println("Author berubah. Awal: " +
				// fotokita.getAuthor() + ", akhir: " +tb_author.getText());
				// xml.setAuthor(tb_author.getText());
				// xml.setLastModified(new Date());
				// }
				// if(!fotokita.getVersion().equals(tb_version.getText())){
				// System.out.println("Versi berubah. Awal: " +
				// fotokita.getVersion() + ", akhir: " +tb_version.getText());
				// xml.setVersion(tb_version.getText());
				// xml.setLastModified(new Date());
				// }
				// if(!fotokita.getPages().getSize().equals(tb_pagesize.getText())){
				// System.out.println("Pages berubah. Awal: " +
				// fotokita.getPages().getSize() + ", akhir: "
				// +tb_pagesize.getText());
				// Pages pages = new Pages();
				// pages.setSize(tb_pagesize.getText());
				// xml.setPages(pages);
				// xml.setLastModified(new Date());
				// }
				// try {
				// marshalling();
				// } catch (JAXBException e1) {
				// // TODO Auto-generated catch block
				// e1.printStackTrace();
				// }
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				// TODO Auto-generated method stub

			}

		});
		dialog_read.pack();
		dialog_read.open();
		while (!dialog_read.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
	}

	public static void update(Display display)
			throws SAXException, IOException, JAXBException, ParserConfigurationException {

		// we need a blank document to store final xml output
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder = dbf.newDocumentBuilder();
		Document document = docBuilder.parse("/Project/COBATA/fotokita3.fbk");

		// create JAXBContext which will be used to create a Binder
		JAXBContext jc = JAXBContext.newInstance(Fotokita.class);

		Binder<Node> binder = jc.createBinder();

		// get xml node from the document
		Node xmlNode = document.getDocumentElement();

		// Returns the updated JAXB object
		Fotokita fk = (Fotokita) binder.updateJAXB(xmlNode);

		// set author
		fk.setAuthor("Joko");
		fk.setLastModified(new Date());

		// update xml node with new data
		xmlNode = binder.updateXML(fk);

		// set node value to the document
		document.setNodeValue(xmlNode.getNodeValue());

		// finally print the edited object on stdout
		TransformerFactory tf = TransformerFactory.newInstance();
		Transformer t;
		try {
			t = tf.newTransformer();
			binder.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			t.setOutputProperty(OutputKeys.INDENT, "yes");
			StreamResult result = new StreamResult(new StringWriter());
			DOMSource source = new DOMSource(document);
			t.transform(source, result);
			String xmlString = result.getWriter().toString();
			System.out.println(xmlString);
			t.transform(new DOMSource(document), new StreamResult("/Project/COBATA/fotokita5.fbk"));
		} catch (TransformerConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (TransformerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static String toBase64(String url) {
		File file = new File(url);
		try {
			// Reading a Image file from file system
			FileInputStream imageInFile = new FileInputStream(file);
			byte imageData[] = new byte[(int) file.length()];
			imageInFile.read(imageData);

			// Converting Image byte array into Base64 String
			String imageDataString = encodeImage(imageData);

			// Converting a Base64 String into Image byte array
			// byte[] imageByteArray = decodeImage(imageDataString);

			// Write a image byte array into file system
			/*
			 * FileOutputStream imageOutFile = new FileOutputStream(
			 * "E:/Project/COBATA/cobaoutput.jpg");
			 */

			// imageOutFile.write(imageByteArray);

			imageInFile.close();
			// imageOutFile.close();
			return imageDataString;
			// System.out.println(imageDataString);
		} catch (FileNotFoundException e) {
			System.out.println("Image not found" + e);
		} catch (IOException ioe) {
			System.out.println("Exception while reading the Image " + ioe);
		}
		return "Image not found";
	}

	public static void toImage(String imageDataString) {
		// Converting a Base64 String into Image byte array
		byte[] imageByteArray = decodeImage(imageDataString);

		// Write a image byte array into file system
		try {
			FileOutputStream imageOutFile = new FileOutputStream("E:/Project/COBATA/cobata.jpeg");
			imageOutFile.write(imageByteArray);
			imageOutFile.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * Encodes the byte array into base64 string
	 *
	 * @param imageByteArray
	 *            - byte array
	 * @return String a {@link java.lang.String}
	 */
	public static String encodeImage(byte[] imageByteArray) {
		return Base64.encodeBase64URLSafeString(imageByteArray);
	}

	/**
	 * Decodes the base64 string into byte array
	 *
	 * @param imageDataString
	 *            - a {@link java.lang.String}
	 * @return byte array
	 */
	public static byte[] decodeImage(String imageDataString) {
		return Base64.decodeBase64(imageDataString);
	}

}